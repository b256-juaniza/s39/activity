const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers.js");
const auth = require("../auth.js");

// Route for creating a course
router.post("/create", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if (data.isAdmin) {

		courseController.addCourse(data.course).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}

});

router.get("/all", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
}) 

router.get("/active", (req, res) => {
	courseController.getAllActiveCourses().then(resultFromController => res.send(resultFromController));
})

router.get("/:courseId", (req, res) => {
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
})

module.exports = router;